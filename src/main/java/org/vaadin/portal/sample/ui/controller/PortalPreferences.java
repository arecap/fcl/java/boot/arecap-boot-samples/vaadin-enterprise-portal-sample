package org.vaadin.portal.sample.ui.controller;


import com.vaadin.spring.endpoint.annotation.GuiComponent;
import com.vaadin.spring.endpoint.annotation.GuiController;
import com.vaadin.spring.endpoint.ui.ApplicationControllerService;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;
import com.vaadin.spring.frontend.ui.FrameContainer;
import org.arecap.vaadin.portal.ui.SideWrapper;
import org.arecap.vaadin.portal.ui.SideWrapperController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.InitBinder;
import org.vaadin.portal.sample.ui.PortalUi;

@GuiController(uri= "/preferences", ui= PortalUi.class)
public class PortalPreferences {

    private static Logger logger = LoggerFactory
            .getLogger(PortalPreferences.class);

    private SideWrapper sideWrapper;

    @Autowired
    private ApplicationControllerService controllerService;

    @InitBinder
    public void onPageInit() {
        controllerService.setId("communitymainlayout");
    }

    @GuiComponent(id="browser", renderOrder = 0)
    public FrameContainer getBrowser() {
        FrameContainer frameContainer = new FrameContainer();
        frameContainer.addStyleName("still");
        frameContainer.setSizeFull();
        return frameContainer;
    }

    @GuiComponent(id="browsercontent", section = "browser", renderOrder = 1)
    public FrameContainer getBroserContent() {
        FrameContainer frameContainer = new FrameContainer();
        frameContainer.setStyleName(StyleConstantUtil.TRANSITIONED);
        frameContainer.removeFrameworkStyle();
        return frameContainer;
    }

    @GuiComponent(id="sidebarwrapper", section = "browser", renderOrder = 2)
    public SideWrapper getSideWrapper() {
        sideWrapper = new SideWrapper("menu", "Menu", "sidebarbutton-menu");
        sideWrapper.addSection("info", "Information", "sidebarbutton-info");
        sideWrapper.addSection("edit", "Edit", "sidebarbutton-editor");
        return sideWrapper;
    }

    @GuiComponent(section = "browser", renderOrder = 3)
    public SideWrapperController getSideWrapperController() {
        return new SideWrapperController(sideWrapper);
    }

}

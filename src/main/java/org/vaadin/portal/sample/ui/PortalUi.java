/*
 * Copyright 2017 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.vaadin.portal.sample.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.endpoint.annotation.GuiApplication;
import com.vaadin.ui.UI;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@GuiApplication
@Widgetset("com.vaadin.spring.frontend.FrontendWidgetSet")
@Theme("community")
public class PortalUi extends UI {


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        //TODO init ui should handle security, errors, etc.
    }

}

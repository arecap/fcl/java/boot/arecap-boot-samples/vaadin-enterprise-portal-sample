package org.vaadin.portal.sample.ui.controller;


import com.vaadin.event.LayoutEvents;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.EventId;
import com.vaadin.spring.endpoint.annotation.EventMapping;
import com.vaadin.spring.endpoint.annotation.GuiComponent;
import com.vaadin.spring.endpoint.annotation.GuiController;
import com.vaadin.spring.endpoint.ui.ApplicationControllerService;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;
import com.vaadin.spring.frontend.ui.Breadcrumb;
import com.vaadin.spring.frontend.ui.CircularNavigation;
import com.vaadin.spring.frontend.ui.FrameContainer;
import com.vaadin.spring.frontend.ui.dto.DtoUtil;
import com.vaadin.spring.frontend.util.FrontendIcons;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.InitBinder;
import org.vaadin.portal.sample.ui.PortalUi;

@GuiController(uri= "/home", ui= PortalUi.class)
public class PortalHome {

    private static Logger logger = LoggerFactory
            .getLogger(PortalHome.class);

    private final FrameContainer sideBarWrapper = new FrameContainer();

    private final FrameContainer menuContent = new FrameContainer();

    private final FrameContainer infoContent = new FrameContainer();

    private final FrameContainer editContent = new FrameContainer();

    @Autowired
    private ApplicationControllerService controllerService;

    @InitBinder
    public void onPageInit() {
        controllerService.setId("communitymainlayout");
    }

    @GuiComponent(id="browser", renderOrder = 0)
    public FrameContainer getBrowser() {
        FrameContainer frameContainer = new FrameContainer();
        frameContainer.addStyleName("still");
        frameContainer.setSizeFull();
        return frameContainer;
    }

    @GuiComponent(id="browsercontent", section = "browser", renderOrder = 1)
    public FrameContainer getBroserContent() {
        FrameContainer frameContainer = new FrameContainer();
        frameContainer.setStyleName(StyleConstantUtil.TRANSITIONED);
        frameContainer.removeFrameworkStyle();
        return frameContainer;
    }

    @GuiComponent(id="breadcrumbs", section = "browser", renderOrder = 2)
    public Breadcrumb getBreadcrumbs() {
        Breadcrumb breadcrumb = new Breadcrumb();
        return breadcrumb;
    }

    @GuiComponent(id="sidebarwrapper", section = "browser", renderOrder = 4)
    public FrameContainer getSideBarWrapper() {
        sideBarWrapper.addStyleName("sidebar-open");
        sideBarWrapper.removeFrameworkStyle();
        return sideBarWrapper;
    }


    @GuiComponent(id="sidebar", section = "sidebarwrapper", renderOrder = 6)
    public FrameContainer getSideBar() {
        FrameContainer sideBar = new FrameContainer();
        sideBar.setStyleName(StyleConstantUtil.getComposedStyle(StyleConstantUtil.TRANSITIONED, "base"));
        sideBar.removeFrameworkStyle();
        return sideBar;
    }

    @GuiComponent(id="sidebar-content", section = "sidebar", renderOrder = 7)
    public FrameContainer getSideContent() {
        FrameContainer sidebarContent = new FrameContainer();
        sidebarContent.removeFrameworkStyle();
        return sidebarContent;
    }

    @GuiComponent(id="edit", section = "sidebar-content", renderOrder = 8)
    public FrameContainer getEditContent() {
        editContent.setStyleName(StyleConstantUtil.getComposedStyle("sidebar-content-content"));
        editContent.removeFrameworkStyle();
        return editContent;
    }

    @GuiComponent(id="info", section = "sidebar-content", renderOrder = 9)
    public FrameContainer getInfoContent() {
        infoContent.setStyleName(StyleConstantUtil.getComposedStyle("description-selected", "sidebar-content-content"));
        infoContent.removeFrameworkStyle();
        return infoContent;
    }

    @GuiComponent(id="menu", section = "sidebar-content", renderOrder = 10)
    public FrameContainer getMenuContent() {
        menuContent.setStyleName(StyleConstantUtil.getComposedStyle("sidebar-content-content", "opencontent"));
        menuContent.removeFrameworkStyle();
        TextField searchBox = new TextField();
        searchBox.setId("searchbox");
        searchBox.setStyleName("gwt-TextBox");
        menuContent.addComponent(searchBox);
        Label inputPrompt= new Label("Search...");
        inputPrompt.setStyleName("gwt-Label inputprompt");
        menuContent.addComponent(inputPrompt);
        return menuContent;
    }

    @GuiComponent(id="side_bar_header_editor", section = "sidebar", renderOrder = 8)
    public FrameContainer getSideBarHeaderEditor() {
        FrameContainer sideBarHeader = new FrameContainer();
        sideBarHeader.setStyleName(StyleConstantUtil.getComposedStyle("sidebar-header","leftfade"));
        sideBarHeader.removeFrameworkStyle();
        sideBarHeader.addComponent(new Label("Edit"));
        return sideBarHeader;
    }

    @GuiComponent(id="side_bar_header_info", section = "sidebar", renderOrder = 9)
    public FrameContainer getSideBarHeaderInfo() {
        FrameContainer sideBarHeader = new FrameContainer();
        sideBarHeader.setStyleName(StyleConstantUtil.getComposedStyle("sidebar-header","leftfade"));
        sideBarHeader.removeFrameworkStyle();
        sideBarHeader.addComponent(new Label("Information"));
        return sideBarHeader;
    }

    @GuiComponent(id="side_bar_header_menu", section = "sidebar", renderOrder = 10)
    public FrameContainer getSideBarHeaderMenu() {
        FrameContainer sideBarHeader = new FrameContainer();
        sideBarHeader.setStyleName(StyleConstantUtil.getComposedStyle("sidebar-header"));
        sideBarHeader.removeFrameworkStyle();
        sideBarHeader.addComponent(new Label("Menu"));
        return sideBarHeader;
    }

    @GuiComponent(id="side_bar_buttons", section = "browser", renderOrder = 5)
    public FrameContainer getSideBarButtons() {
        FrameContainer sideBarButtons = new FrameContainer();
        sideBarButtons.setStyleName(StyleConstantUtil.getComposedStyle("sidebarbuttons", StyleConstantUtil.TRANSITIONED));
        sideBarButtons.removeFrameworkStyle();
        return sideBarButtons;
    }

    @GuiComponent(id="side_bar_button_editor", section = "side_bar_buttons", renderOrder = 6)
    public FrameContainer getSideBarButtonEditor() {
        FrameContainer sideBarButton = new FrameContainer();
        sideBarButton.setStyleName(StyleConstantUtil.getComposedStyle("sidebarbutton","sidebarbutton-editor"));
        sideBarButton.removeFrameworkStyle();
        return sideBarButton;
    }

    @GuiComponent(id="side_bar_button_info", section = "side_bar_buttons", renderOrder = 7)
    public FrameContainer getSideBarButtonInfo() {
        FrameContainer sideBarButton = new FrameContainer();
        sideBarButton.setStyleName(StyleConstantUtil.getComposedStyle("sidebarbutton","sidebarbutton-info"));
        sideBarButton.removeFrameworkStyle();
        return sideBarButton;
    }

    @GuiComponent(id="side_bar_button_menu", section = "side_bar_buttons", renderOrder = 8)
    public FrameContainer getSideBarButtonMenu() {
        FrameContainer sideBarButton = new FrameContainer();
        sideBarButton.setStyleName(StyleConstantUtil.getComposedStyle("sidebarbutton","sidebarbutton-menu", StyleConstantUtil.SELECTED));
        sideBarButton.removeFrameworkStyle();
        return sideBarButton;
    }

    @GuiComponent(id = "navigator", section = "browsercontent", renderOrder = 3)
    public CircularNavigation getCircularNavigation() {
        CircularNavigation circularNavigation = new CircularNavigation();
        circularNavigation.setContentIcon(new FrontendIcons(0xE003, ""));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.ARCHIVE, "archive", "archive",
                        NavigatorUtils.getControllerUrl(PortalHome.class)));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.ARCHIVES, "archives", "archives",
                        NavigatorUtils.getControllerUrl(PortalPreferences.class)));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.BAR_CHART_H, "bar_chart_h", "bar_chart_h",
                        NavigatorUtils.getControllerUrl(PortalPreferences.class)));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.AUTOMATION, "automation", "automation",
                        NavigatorUtils.getControllerUrl(PortalPreferences.class)));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.ARCHIVE, "archive", "archive",
                        NavigatorUtils.getControllerUrl(PortalPreferences.class)));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.ARCHIVES, "archives", "archives",
                        NavigatorUtils.getControllerUrl(PortalPreferences.class)));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.BAR_CHART_H, "bar_chart_h", "bar_chart_h",
                        NavigatorUtils.getControllerUrl(PortalPreferences.class)));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.AUTOMATION, "automation", "automation",
                        NavigatorUtils.getControllerUrl(PortalPreferences.class)));
        circularNavigation.addNavigationIcon(DtoUtil
                .getContentIconRef(VaadinIcons.AUTOMATION, "automation", "automation",
                        NavigatorUtils.getControllerUrl(PortalPreferences.class)));
        return circularNavigation;
    }

    @EventMapping(section = "side_bar_button_menu", eventId = EventId.LAYOUT_CLICK_EVENT_IDENTIFIER, event = LayoutEvents.LayoutClickEvent.class)
    public void siderBarMenuButtonClick() {
        if(sideBarWrapper.getStyleName().contains("sidebar-open")) {
            sideBarWrapper.setStyleName("");
        } else {
            sideBarWrapper.addStyleName("sidebar-open");
        }
    }

}

package org.vaadin.portal.sample.catalog;



//import javax.persistence.Id;

public class ActionItemContainer {


    private String graphics;

//    @Id
    private String label;

    private ActionItemContainer parent;

    public String getGraphics() {
        return graphics;
    }

    public void setGraphics(String graphics) {
        this.graphics = graphics;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ActionItemContainer getParent() {
        return parent;
    }

    public void setParent(ActionItemContainer parent) {
        this.parent = parent;
    }
}

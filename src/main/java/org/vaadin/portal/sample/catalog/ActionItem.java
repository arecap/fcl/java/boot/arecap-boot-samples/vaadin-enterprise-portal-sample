package org.vaadin.portal.sample.catalog;


//import javax.persistence.Id;

public class ActionItem  {


    private String graphics;

//    @Id
    private String label;

    private String action;

    private ActionItemContainer container;

    public String getGraphics() {
        return graphics;
    }

    public void setGraphics(String graphics) {
        this.graphics = graphics;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public ActionItemContainer getContainer() {
        return container;
    }

    public void setContainer(ActionItemContainer container) {
        this.container = container;
    }
}
